
import random
import sys

import numpy as np


# regressão linear
from src.network import Network

theta0 = 0.0
theta1 = 0.0
alpha = 0.1
M = 2000
minimum_improvement = 0.0001

def normalize_dataset(dataset):
    attributes = len(dataset[0][0])
    outputs = len(dataset[0][1])
    input_ranges = []
    for i in range(attributes):
        input_ranges.append([float("inf"), float("-inf")])
    output_ranges = []
    for i in range(outputs):
        output_ranges.append([float("inf"), float("-inf")])
    for data in dataset:
        for i in range(attributes):
            current_value = data[0][i]
            input_ranges[i][0] = min(input_ranges[i][0], current_value)
            input_ranges[i][1] = max(input_ranges[i][1], current_value)
        for i in range(outputs):
            current_value = data[1][i]
            input_ranges[i][0] = min(input_ranges[i][0], current_value)
            input_ranges[i][1] = max(input_ranges[i][1], current_value)
    for data in dataset:
        for i in range(attributes):
            data[0][i] = np.interp(data[0][i], input_ranges[i], [0, 1])
        for i in range(outputs):
            data[1][i] = np.interp(data[1][i], output_ranges[i], [0, 1])

if __name__ == "__main__":
    n_f = sys.argv[1]  # network structure file
    network_lambda = 0
    layer_sizes = []
    with open(n_f) as network_file:
        lines = network_file.readlines()
        network_lambda = float(lines[0])
        for line in lines[1:]:
            layer_sizes.append(int(line))

    print("regularization lambda:", network_lambda)
    print("network layers:", layer_sizes)

    d_f = sys.argv[2]  # network dataset file
    dataset = []
    with open(d_f) as dataset_file:
        for line in dataset_file:
            attributes = line.split(';')
            inputs = []
            for value in attributes[0].split(','):
                inputs.append(float(value))
            outputs = []
            for value in attributes[1].split(','):
                outputs.append(float(value))
            instance = [inputs, outputs]
            dataset.append(instance)
    print("network training data:\n", dataset)
    normalize_dataset(dataset)
    print("network normalised training data:\n", dataset)

    layers_v = []
    if len(sys.argv) > 3:
        w_f = sys.argv[3]  # network initial weights file
        with open(w_f) as weight_file:
            for line in weight_file:
                neurons = line.split(';')
                neurons_v = []
                for neuron in neurons:
                    weights = neuron.split(',')
                    weights_v = []
                    for weight in weights:
                        weights_v.append(float(weight))
                    neurons_v.append(weights_v)
                layers_v.append(np.array(neurons_v))
        print("network layers initial weights:\n", layers_v)
    else:
        for idx, layer in enumerate(layer_sizes[:-1]):
            neurons_v = []
            for x in range(0, layer_sizes[idx+1]):
                weights_v = []
                for y in range(0, layer + 1):
                    weights_v.append(random.triangular(-1, 1, 0))
                neurons_v.append(weights_v)
            layers_v.append(np.array(neurons_v))

    network = Network(layer_sizes, layers_v, network_lambda)

    x = 0
    J_old = 99999
    J = 0
    while x < M and abs(J_old - J) > minimum_improvement:
        # print("measuring error J of the network")
        J_old = J
        J = 0
        S = 0
        # forward propagation
        a = []
        for idx, data in enumerate(dataset):
            # print("processing training example", idx + 1)
            input = data[0][:]
            output = data[1][:]
            j = network.get_error_for_input(input, output)
            # print("a", network.a)
            # print("z", network.z)
            # print("f(x)", network.outputs)
            # print("J(", idx + 1, ") = ", j)
            J += j
            # print()

        J /= len(dataset)
        print("total network error:", J)

        S = network.get_network_S()
        S *= network.network_lambda / (len(dataset) * 2)
        print("network error after regularization:", J + S)

        # print()

        # backpropagation (batch training)
        D = {}
        for idx, data in enumerate(dataset):
            # print("back propagating training example", idx + 1)
            input = data[0][:]
            expected_output = data[1][:]
            delta, d = network.backward(input, expected_output)
            for k in range(len(network.weights), 0, -1):
                if k in D:
                    D[k] += d[k]
                else:
                    D[k] = d[k]
                # print("D[", k, "]:", d[k])

            ()

        # calcule normalization
        P = {}
        for k in range(len(network.weights), 0, -1):
            weights_to_regularize = np.matrix(network.weights[k - 1][:])
            weights_to_regularize[:, 0] = np.zeros(len(weights_to_regularize[0]))
            P[k] = network.network_lambda * weights_to_regularize
            D[k] = (1.0 / len(dataset)) * (D[k] + P[k])

        # print("Complete dataset processed, final gradients:")
        # for k in range(1, len(network.weights) + 1):
            # print("D[", k, "]:", D[k])
        # print()

        network.update_weights(D, alpha)
    print("updated weights:\n", network.weights)

