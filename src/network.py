import numpy as np


class Network:
    def __init__(self, structure, initial_weights, network_lambda):
        # store intermediate state
        self.z = {}

        # network structure
        self.structure = structure

        # network weights
        self.weights = initial_weights

        # Normalization lambda
        self.network_lambda = network_lambda

        # output vector
        self.outputs = None

        # activations
        self.a = {}

        self.vector_sigmoid = np.vectorize(self.sigmoid)

    @staticmethod
    def sigmoid(z):
        sigm = 1. / (1. + np.exp(-z))
        return sigm

    def forward(self, input_data):
        self.a[0] = np.array(input_data)
        self.a[0] = np.insert(self.a[0], 0, 1)  # add bias element
        for idx in range(1, len(self.weights)):
            np_theta = np.array(self.weights[idx - 1])

            self.z[idx] = np_theta @ self.a[idx - 1]

            self.a[idx] = self.vector_sigmoid(self.z[idx])
            self.a[idx] = np.insert(self.a[idx], 0, 1)

        np_theta = np.array(self.weights[-1])

        self.z[len(self.weights)] = np_theta @ self.a[(len(self.weights) - 1)]
        self.a[len(self.weights)] = self.vector_sigmoid(self.z[len(self.weights)])
        self.outputs = self.vector_sigmoid(self.z[len(self.weights)])
        return self.outputs

    def backward(self, input_data, expected_outuput_data):
        output = self.forward(input_data)
        delta = {len(self.weights) + 1: output - expected_outuput_data}
        # print("delta[", len(self.weights) + 1, "]:", delta[len(self.weights) + 1])
        for k in range(len(self.weights), 1, -1):
            delta_k = \
                np.transpose(self.weights[k - 1]) @ delta[k + 1] \
                * self.a[k - 1] \
                * (np.ones(len(self.a[k - 1])) - self.a[k - 1])
            delta[k] = delta_k[1:]
            # print("delta[", k, "]:", delta[k])
        d = {}
        for k in range(len(self.weights), 0, -1):
            transpose = np.array([self.a[k - 1]]).T
            d[k] = (delta[k + 1] * transpose).T
            # print("D[", k, "]:", d[k])
        return delta, d

    def get_error_for_input(self, input_data, expected_output_data):
        f_x = self.forward(input_data)
        y = np.array(expected_output_data)
        j_vector = -y * np.log(f_x) - (np.ones(len(y)) - y) * np.log(np.ones(len(y)) - f_x)
        j = np.sum(j_vector)
        return j

    def get_network_S(self):
        s = 0
        for weight_layer in self.weights:
            for weight_neuron in weight_layer:
                for weight in weight_neuron[1:]:
                    s += weight ** 2.0
        return s

    def update_weights(self, D, alpha):
        # print("original weights:\n", self.weights)
        for k in range(len(self.weights)  , 0, -1):
            self.weights[k-1] = np.asarray(self.weights[k-1][:] - (alpha * D[k]))
        # print("updated weights:\n", self.weights)

